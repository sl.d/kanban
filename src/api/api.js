import axios from "axios";
import {showError} from "../services/notifications.service";

const baseURL = `/rest`;

const axiosInstance = axios.create({
  baseURL,
});

axiosInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  ({response}) => {
    showError();
    return Promise.reject(response);
  }
);

export default axiosInstance;
