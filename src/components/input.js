import React from "react";
import {Input} from "antd";

// eslint-disable-next-line react/display-name
export const InputComponent = ({
  handleChange,
  placeholder,
  type,
  selectClassName,
  width,
  disabled,
  value,
}) => {
  return (
    <div className={`input-component ${selectClassName}`}>
      <Input
        type={type | "text"}
        placeholder={placeholder}
        onChange={handleChange}
        width={width}
        disabled={disabled}
        value={value}
      />
    </div>
  );
};
