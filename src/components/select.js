import React from "react";
import {Select} from "antd";
const {Option} = Select;

export const SelectComponent = ({
  handleChange,
  placeholder,
  data,
  value,
  labelInValue,
  disabled,
  allowClear,
  onBlur,
}) => {
  return (
    <Select
      value={value}
      placeholder={placeholder}
      bordered={false}
      onChange={handleChange}
      disabled={disabled}
      labelInValue={labelInValue}
      allowClear={allowClear}
      onBlur={onBlur}
    >
      {data.map((item) => (
        <Option key={item.value} className={item.className} title={item.label}>
          {item.label}
        </Option>
      ))}
    </Select>
  );
};
