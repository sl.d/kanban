import React from "react";
import {Input} from "antd";
const {TextArea} = Input;

// eslint-disable-next-line react/display-name
export const TextareaComponent = ({
  handleChange,
  placeholder,
  selectClassName,
  width,
  disabled,
  value,
}) => {
  return (
    <div className={`input-component ${selectClassName}`}>
      <TextArea
        placeholder={placeholder}
        onChange={handleChange}
        width={width}
        disabled={disabled}
        rows={3}
        value={value}
      />
    </div>
  );
};
