import React from "react";
import ReactDOM from "react-dom";
import Routes from "./routes";
import "./styles/ant.less";
import "./styles/main.scss";

ReactDOM.render(<Routes />, document.getElementById("root"));
