import React from "react";

export const NotFound = () => {
  return (
    <div className="in-work-page">
      <h1>Страница не найдена</h1>
    </div>
  );
};
