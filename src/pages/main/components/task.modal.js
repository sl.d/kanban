import React from "react";
import {inject, observer} from "mobx-react";
import {Button, Modal, Spin} from "antd";
import AddFormComponent from "./add-form.component";
import {useHistory} from "react-router-dom";

const TaskModal = inject("store")(
  observer(({store: {tasks}}) => {
    const history = useHistory();
    const {currentTask, addTypeTask, modalIsLoading, showModal} = tasks;
    const [form, setForm] = React.useState({});

    const clearStoreForModalAndUrl = React.useCallback(() => {
      tasks.setCurrentTask(null);
      tasks.setAddTypeTask(null);
      tasks.setShowModal(false);
      history.push({search: null});
    }, []);

    const handleCancel = React.useCallback(() => {
      clearStoreForModalAndUrl();
    }, [tasks]);

    const handleOk = React.useCallback(() => {
      const status = addTypeTask ? addTypeTask : form.status;
      const id = currentTask?.id;
      clearStoreForModalAndUrl();
      tasks.addTask({...form, status, id});
    }, [addTypeTask, currentTask, form]);

    const onInputChange = React.useCallback(
      (form) => {
        setForm(form);
      },
      [form]
    );

    return (
      <Modal
        width={620}
        title={addTypeTask ? "Добавление карточки" : "Редактирование карточки"}
        visible={showModal}
        onCancel={handleCancel}
        destroyOnClose={true}
        footer={[
          <Button key="submit" type="primary" onClick={handleOk}>
            {currentTask ? "Сохранить" : "Добавить"}
          </Button>,
        ]}
      >
        {modalIsLoading ? (
          <div className="spin-center">
            <Spin />
          </div>
        ) : (
          <AddFormComponent {...currentTask} onInputChange={onInputChange} />
        )}
      </Modal>
    );
  })
);

export default React.memo(TaskModal);
