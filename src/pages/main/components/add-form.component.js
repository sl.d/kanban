import React, {useEffect} from "react";
import {InputComponent} from "../../../components/input";
import {TextareaComponent} from "../../../components/textarea";
import {SelectComponent} from "../../../components/select";
import {statusesOptions} from "../../../consts/status.const";

const AddFormComponent = ({id, title, desc, status, onInputChange}) => {
  const statusForSelect = React.useMemo(
    () => statusesOptions.find((item) => item.value === status),
    [status]
  );

  const [titleValue, setTitleValue] = React.useState(title);
  const [descValue, setDescValue] = React.useState(desc);
  const [statusValue, setStatusValue] = React.useState(statusForSelect);

  useEffect(() => {
    onInputChange({
      title: titleValue,
      desc: descValue,
      status: statusValue?.value,
    });
  }, [titleValue, descValue, statusValue]);

  return (
    <div className="add-form">
      <div>
        <InputComponent
          placeholder="Заголовок"
          value={titleValue}
          handleChange={(value) => setTitleValue(value.target.value)}
        />
      </div>
      <div>
        <TextareaComponent
          placeholder="Описание"
          value={descValue}
          handleChange={(value) => setDescValue(value.target.value)}
        />
      </div>
      {id && (
        <div>
          <SelectComponent
            data={statusesOptions}
            value={statusValue}
            labelInValue={true}
            handleChange={setStatusValue}
            placeholder="Статус"
          />
        </div>
      )}
    </div>
  );
};

export default React.memo(AddFormComponent);
