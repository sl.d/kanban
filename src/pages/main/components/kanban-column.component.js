import React from "react";
import TaskComponent from "./task.component";
import {Button} from "antd";
import PlusOutlined from "@ant-design/icons/lib/icons/PlusOutlined";
import {dndAcceptType} from "../../../consts/kanban.const";
import {useDrop} from "react-dnd";

const KanbanColumnComponent = ({label, value, data, tasksStore}) => {
  const [{isOver}, drop] = useDrop({
    accept: dndAcceptType.TASK,
    drop: () => ({name: value}),
    collect: (monitor) => ({
      isOver: monitor.isOver(),
    }),
  });

  const addTask = React.useCallback(() => {
    tasksStore.setAddTypeTask(value);
    tasksStore.setShowModal(true);
  }, [value, tasksStore.setAddTypeTask]);

  return (
    <div className={`kanban-column ${isOver ? "is-over" : ""}`} ref={drop}>
      <div>
        <div className="kanban-column--title">{label}</div>
        <div className="kanban-column--tasks">
          {data.length ? (
            data.map((task) => {
              return <TaskComponent key={task.id} {...task} />;
            })
          ) : (
            <div className="kanban-column--empty">Список пуст</div>
          )}
        </div>
      </div>
      <div className="kanban-column--add-btn">
        <Button type="text" icon={<PlusOutlined />} onClick={addTask}>
          Добавить ещё одну карточкук
        </Button>
      </div>
    </div>
  );
};

export default React.memo(KanbanColumnComponent);
