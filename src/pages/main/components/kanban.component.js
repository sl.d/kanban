import React, {useEffect} from "react";
import {inject, observer} from "mobx-react";
import {statusesOptions} from "../../../consts/status.const";
import {Spin} from "antd";
import {useHistory} from "react-router-dom";
import {DndProvider} from "react-dnd";
import {HTML5Backend} from "react-dnd-html5-backend";
import KanbanColumnComponent from "./kanban-column.component";

export const KanbanComponent = inject("store")(
  observer(({store: {tasks}}) => {
    const {location} = useHistory();

    useEffect(() => {
      tasks.getTasks();

      const params = new URLSearchParams(location.search);
      const taskId = params.get("task");
      if (taskId) {
        tasks.getTaskById(taskId);
        tasks.setShowModal(true);
      }
    }, []);

    return (
      <DndProvider backend={HTML5Backend}>
        <Spin spinning={tasks.isLoading}>
          <div className="kanban-component">
            {statusesOptions.map((item) => {
              const tasksList = tasks.tasks.filter((task) => task.status === item.value);

              return (
                <KanbanColumnComponent
                  key={item.value}
                  {...item}
                  data={tasksList}
                  tasksStore={tasks}
                />
              );
            })}
          </div>
        </Spin>
      </DndProvider>
    );
  })
);
