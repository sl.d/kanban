import React from "react";
import {inject, observer} from "mobx-react";
import {useHistory} from "react-router-dom";
import {useDrag} from "react-dnd";
import {dndAcceptType} from "../../../consts/kanban.const";

const TaskComponent = inject("store")(
  observer(({store: {tasks}, id, title, status}) => {
    const history = useHistory();
    const {tasks: dataTasks} = tasks;

    const [{isDragging}, drag] = useDrag({
      item: {
        name: id,
        type: dndAcceptType.TASK,
      },
      collect: (monitor) => ({
        isDragging: monitor.isDragging(),
      }),
      end: (item, monitor) => {
        if (monitor.getDropResult()) {
          const {name: dropColumnName} = monitor.getDropResult();
          if (status !== dropColumnName) {
            tasks.setTasks(
              dataTasks.map((item) =>
                item.id === id ? {...item, status: dropColumnName} : item
              )
            );
            tasks.addTask({id, status: dropColumnName}, false);
          }
        }
      },
    });

    const opacity = isDragging ? 0.5 : 1;

    const openTask = React.useCallback(() => {
      tasks.getTaskById(id);
      tasks.setShowModal(true);
      history.push({search: `?task=${id}`});
    }, [id, history]);

    return (
      <div ref={drag} className="task" onClick={openTask} style={{opacity}}>
        {title ? title : `Карточка №${id}`}
      </div>
    );
  })
);

export default React.memo(TaskComponent);
