import React from "react";
import {KanbanComponent} from "./components/kanban.component";
import TaskModal from "./components/task.modal";
// local files

export const MainPage = () => {
  return (
    <>
      <KanbanComponent />
      <TaskModal />
    </>
  );
};
