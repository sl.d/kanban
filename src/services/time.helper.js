import moment from "moment";
import "moment/locale/ru";

export const formats = {
  DATE_DOT: "DD.MM.YYYY",
  TIME_NO_SECOND: "HH:mm",
};

export const getCurrentMomentDate = (date) => {
  return moment(date, formats.DATE_DOT);
};
