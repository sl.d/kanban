import TasksStore from "./tasks/tasks.store";

class RootStore {
  constructor() {
    this.tasks = TasksStore;
  }
}

export default new RootStore();
