import {decorate, observable} from "mobx";
// local files
import {TasksAction} from "./tasks.action";

class TasksStore extends TasksAction {
  tasks = [];
  currentTask = null;
  isLoading = false;
  addTypeTask = null;
  modalIsLoading = null;
  showModal = false;
}

// eslint-disable-next-line no-class-assign
TasksStore = decorate(TasksStore, {
  tasks: observable,
  currentTask: observable,
  isLoading: observable,
  addTypeTask: observable,
  modalIsLoading: observable,
  showModal: observable,
});

export default new TasksStore();
