import axiosInstance from "../../api/api";
import {addSuccess} from "../../services/notifications.service";

export class TasksAction {
  getTasks() {
    this.setIsLoading(true);
    axiosInstance
      .get("/tasks")
      .then(({data}) => {
        this.setTasks(data);
      })
      .finally(() => this.setIsLoading(false));
  }

  addTask(params, refreshTasks = true) {
    const {id} = params;
    this.setIsLoading(true);
    if (id) {
      axiosInstance
        .patch(`/tasks/${id}`, params)
        .then(() => {
          if (refreshTasks) {
            addSuccess("Карточка обновлена");
            this.getTasks();
          }
        })
        .finally(() => this.setIsLoading(false));
    } else {
      axiosInstance
        .post("/tasks", params)
        .then(() => {
          addSuccess();
          this.getTasks();
        })
        .finally(() => this.setIsLoading(false));
    }
  }

  getTaskById(id) {
    this.setModalIsLoading(true);
    axiosInstance
      .get(`/tasks/${id}`)
      .then(({data}) => {
        this.setCurrentTask(data);
      })
      .finally(() => this.setModalIsLoading(false));
  }

  setTasks(tasks) {
    this.tasks = tasks;
  }

  setCurrentTask(data) {
    this.currentTask = data;
  }

  setIsLoading(value) {
    this.isLoading = value;
  }

  setAddTypeTask(type) {
    this.addTypeTask = type;
  }

  setModalIsLoading(value) {
    this.modalIsLoading = value;
  }

  setShowModal(value) {
    this.showModal = value;
  }
}
