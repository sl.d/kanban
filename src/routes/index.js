import React from "react";
import {Provider} from "mobx-react";
import {HashRouter, Route, Switch} from "react-router-dom";
import store from "../store";
import {MainPage} from "../pages/main/Main";
import {NotFound} from "../pages/not-found/not-found";

export default function Routes() {
  return (
    <Provider store={store}>
      <HashRouter>
        <Switch>
          <Route path="/" exact component={MainPage} />
          <Route component={NotFound} />
        </Switch>
      </HashRouter>
    </Provider>
  );
}
