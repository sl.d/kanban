export const CONST_STATUSES = {
  IN_WORK: "IN_WORK",
  IN_TEST: "IN_TEST",
  DONE: "DONE",
};

export const statusesOptions = [
  {
    value: CONST_STATUSES.IN_WORK,
    label: "В работе",
  },
  {
    value: CONST_STATUSES.IN_TEST,
    label: "На проверке",
  },
  {
    value: CONST_STATUSES.DONE,
    label: "Выполнено",
  },
];
